use std::fs;

fn read_input(path: &str) -> String {
    fs::read_to_string(path).expect("Shit! Couldn't find file.")
}

fn get_row(input: &str) -> usize {
    let mut rows: Vec<usize> = (0..128).collect();
    for c in input.chars() {
        let start = rows[0];
        let end = rows[rows.len() - 1];
        let mid = (start + end) / 2 + 1;

        match c {
            'F' => rows = (start..mid).collect(),
            'B' => rows = (mid..=end).collect(),
            _ => (),
        }
    }
    rows[0]
}

fn get_column(input: &str) -> usize {
    let mut columns: Vec<usize> = (0..8).collect();
    for c in input.chars() {
        let start = columns[0];
        let end = columns[columns.len() - 1];
        let mid = (start + end) / 2 + 1;

        match c {
            'L' => columns = (start..mid).collect(),
            'R' => columns = (mid..=end).collect(),
            _ => (),
        }
    }
    columns[0]
}

fn parse_input(input: &str) -> usize {
    let row_input = &input[..7];
    let column_input = &input[7..];

    let column = get_column(&column_input);
    let row = get_row(&row_input);

    row * 8 + column
}

fn part1() -> usize {
    let content = read_input("./input.txt");
    content.lines().map(|l| parse_input(l)).max().unwrap()
}

fn part2() -> usize {
    let content = read_input("./input.txt");
    let ids: Vec<usize> = content.lines().map(|l| parse_input(l)).collect();
    let seat_id_range: Vec<usize> =
        (ids.iter().min().unwrap().clone()..ids.iter().max().unwrap().clone()).collect();

    for potential_id in seat_id_range.iter() {
        let mut is_in_ids = false;
        for id in ids.iter() {
            if potential_id == id {
                is_in_ids = true;
                break;
            }
        }

        if !is_in_ids {
            return *potential_id;
        }
    }

    0 // Should probably turn this into a Result type...
}

fn main() {
    let highest_id = part1();
    println!("Part 1: {}", highest_id);

    let seat_id = part2();
    println!("Part 2: {}", seat_id);
}

#[test]
fn test_get_row() {
    let input = "FBFBBF";
    let row = get_row(&input);

    assert_eq!(row, 44);

    let input = "BFFFBBF";
    let row = get_row(&input);

    assert_eq!(row, 70);

    let input = "FFFBBBF";
    let row = get_row(&input);

    assert_eq!(row, 14);

    let input = "BBFFBBF";
    let row = get_row(&input);

    assert_eq!(row, 102);
}

#[test]
fn test_get_column() {
    let input = "RRR";
    let column = get_column(&input);

    assert_eq!(column, 7);

    let input = "RRR";
    let column = get_column(&input);

    assert_eq!(column, 7);

    let input = "RLL";
    let column = get_column(&input);

    assert_eq!(column, 4);
}

#[test]
fn test_parse_input() {
    let input = "FBFBBFFRLR";
    let seat_id = parse_input(input);

    assert_eq!(seat_id, 357);
}
