use std::{collections::HashSet, fs};

fn process_group(group: &str) -> usize {
    let mut answers: HashSet<char> = HashSet::new();
    for line in group.lines() {
        answers.extend(line.chars());
    }

    answers.len()
}

fn process_group_all(group: &str) -> usize {
    let mut answers: HashSet<char> = HashSet::new();
    let mut lines = group.lines();

    // Build initial set
    answers.extend(lines.next().unwrap().chars());

    for line in lines {
        let new: HashSet<char> = line.chars().collect();

        answers = answers.intersection(&new).map(|c| c.clone()).collect();
    }
    answers.len()
}

fn read_file(file: &str) -> String {
    // TODO: Figure out how to stream this with BufReader so I can keep up with
    //       the Golang cool kids.
    fs::read_to_string(file).expect("Could not read file!")
}

fn part1() -> usize {
    let input = read_file("input.txt");
    return input.split("\n\n").map(|g| process_group(g)).sum();
}

fn part2() -> usize {
    let input = read_file("input.txt");
    return input.split("\n\n").map(|g| process_group_all(g)).sum();
}

fn main() {
    let sum_of_yes = part1();
    println!("Part 1 -- All 'yes' answers: {}", sum_of_yes);

    let sum_of_all_yes = part2();
    println!("Part 2 -- All 'yes' answers: {}", sum_of_all_yes);
}

#[test]
fn test_process_group() {
    let input = "abc";
    let answers = process_group(input);

    assert_eq!(answers, 3);

    let input = "ab
ac";
    let answers = process_group(input);
    assert_eq!(answers, 3);
}

#[test]
fn test_process_group_all() {
    let input = "abc";
    let answers = process_group_all(input);

    assert_eq!(answers, 3);

    let input = "a
b
c";
    let answers = process_group_all(input);

    assert_eq!(answers, 0);

    let input = "ab
ac";
    let answers = process_group_all(input);
    assert_eq!(answers, 1);

    let input = "a
a
a
a";
    let answers = process_group_all(input);
    assert_eq!(answers, 1);

    let input = "b";
    let answers = process_group_all(input);
    assert_eq!(answers, 1);
}
