use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn input_to_vec(path: &str) -> Vec<i32> {
    let mut numbers: Vec<i32> = Vec::new();

    let contents = File::open(path).unwrap();
    let reader = BufReader::new(contents);

    for line_ in reader.lines() {
        let line = line_.unwrap();
        let number = line.parse::<i32>().unwrap();
        numbers.push(number);
    }
    numbers
}

fn part1(path: &str) {
    let numbers = input_to_vec(path);
    for num1 in numbers.iter() {
        for num2 in numbers.iter() {
            if num1 + num2 == 2020 {
                println!("{} x {} = {}", num1, num2, num1 * num2);
                return;
            }
        }
    }
}

fn part2(path: &str) {
    let numbers = input_to_vec(path);
    for num1 in numbers.iter() {
        for num2 in numbers.iter() {
            for num3 in numbers.iter() {
                if num1 + num2 + num3 == 2020 {
                    println!("{} x {} x {} = {}", num1, num2, num3, num1 * num2 * num3);
                    return;
                }
            }
        }
    }
}

fn main() {
    let file = "./input.txt";
    part1(file);
    part2(file);
}
