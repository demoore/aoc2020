use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[allow(dead_code)]
fn input_to_lines(path: &str) -> Vec<String> {
    let contents = File::open(path).unwrap();
    let reader = BufReader::new(contents);

    let mut contents: Vec<String> = Vec::new();
    for line_ in reader.lines() {
        let line = line_.unwrap();
        contents.push(line);
    }
    contents
}

#[derive(Debug)]
struct Toboggan {
    position: usize,
    trees_hit: usize,
    slope_right: usize,
    slope_down: usize,
}

impl Toboggan {
    pub fn new(right: usize, down: usize) -> Self {
        Self {
            position: 0,
            trees_hit: 0,
            slope_right: right,
            slope_down: down,
        }
    }

    pub fn move_on_map(&mut self, row1: String, row2: String) {
        let row2_chars: Vec<char> = row2.chars().collect();

        self.position += self.slope_right;
        if self.position >= row1.len() {
            self.position = self.position - row1.len();
        }

        if row2_chars[self.position] == '#' {
            self.trees_hit += 1;
        }
    }

    pub fn traverse_map(&mut self, map: Vec<String>) {
        let clean_map: Vec<String> = map.into_iter().step_by(self.slope_down).collect();

        let total = clean_map.len();

        for (i, l) in clean_map.iter().enumerate() {
            let next = i + 1;
            if next == total {
                break;
            }
            let l2 = &clean_map[next];
            println!("line {:?}", l2);
            self.move_on_map(l.to_owned(), l2.to_string());
        }
    }
}

fn part1() -> usize {
    let mut t = Toboggan::new(3, 1);

    let map = input_to_lines("./input.txt");
    t.traverse_map(map);
    t.trees_hit
}

fn part2() -> usize {
    let rules = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let mut total = 1;

    for r in rules.iter() {
        let map = input_to_lines("./input.txt");
        let mut t = Toboggan::new(r.0, r.1);

        t.traverse_map(map);
        println!("total: {}\ttrees hit: {}", total, t.trees_hit);
        total *= t.trees_hit;
    }
    total
}

fn main() {
    println!("Part 1: {}", part1());

    println!("Part 2: {}", part2());
}

#[test]
fn test_map_jump() {
    let mut t = Toboggan::new(3, 1);
    let row1 = "..##.......";
    let row2 = "#...#...#..";

    t.move_on_map(row1.to_string(), row2.to_string());
    assert_eq!(t.trees_hit, 0);

    let row3 = ".#....#..#.";
    t.move_on_map(row2.to_string(), row3.to_string());
    assert_eq!(t.trees_hit, 1);
}

#[test]
fn test_traverse_map() {
    let map_str = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";

    let map = map_str.lines().map(|s| s.to_string()).collect();

    let mut t = Toboggan::new(3, 1);
    t.traverse_map(map);
    let t1 = t.trees_hit;
    assert_eq!(t1, 7);

    let mut t = Toboggan::new(1, 1);
    let map = map_str.lines().map(|s| s.to_string()).collect();
    t.traverse_map(map);
    let t2 = t.trees_hit;
    assert_eq!(t2, 2);

    let mut t = Toboggan::new(5, 1);
    let map = map_str.lines().map(|s| s.to_string()).collect();
    t.traverse_map(map);
    let t3 = t.trees_hit;
    assert_eq!(t3, 3);

    let mut t = Toboggan::new(7, 1);
    let map = map_str.lines().map(|s| s.to_string()).collect();
    t.traverse_map(map);
    let t4 = t.trees_hit;
    assert_eq!(t4, 4);

    let mut t = Toboggan::new(1, 2);
    let map = map_str.lines().map(|s| s.to_string()).collect();
    t.traverse_map(map);
    let t5 = t.trees_hit;
    assert_eq!(t5, 2, "Failed down 2");

    let total = t1 * t2 * t3 * t4 * t5;
    assert_eq!(total, 336);
}

#[test]
fn test_product_of_rules() {
    let map_str = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";

    let rules = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let mut total = 1;

    for r in rules.iter() {
        let mut t = Toboggan::new(r.0, r.1);
        let map = map_str.lines().map(|s| s.to_string()).collect();
        t.traverse_map(map);
        total *= t.trees_hit;
    }

    assert_eq!(total, 336);
}
