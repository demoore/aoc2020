use std::fs;

#[derive(Debug)]
struct Passport {
    birth_year: String,
    issue_year: String,
    expiration_year: String,
    height: String,
    hair_color: String,
    eye_color: String,
    passport_id: String,
    country_id: String,
}

impl Passport {
    pub fn empty() -> Self {
        Self {
            birth_year: "".to_string(),
            issue_year: "".to_string(),
            expiration_year: "".to_string(),
            height: "".to_string(),
            hair_color: "".to_string(),
            eye_color: "".to_string(),
            passport_id: "".to_string(),
            country_id: "North Pole".to_string(),
        }
    }

    fn is_filled_out(&self) -> bool {
        return self.birth_year != ""
            && self.issue_year != ""
            && self.expiration_year != ""
            && self.height != ""
            && self.hair_color != ""
            && self.eye_color != ""
            && self.passport_id != "";
    }

    fn is_correct(&self) -> bool {
        self.is_valid_birth_year()
            && self.is_valid_issue_year()
            && self.is_valid_expiry_year()
            && self.is_valid_height()
            && self.is_valid_hair_color()
            && self.is_valid_eye_color()
            && self.is_valid_pid()
    }

    fn add_record(&mut self, record: String, value: String) {
        match record.as_str() {
            "byr" => self.birth_year = value,
            "iyr" => self.issue_year = value,
            "eyr" => self.expiration_year = value,
            "hgt" => self.height = value,
            "hcl" => self.hair_color = value,
            "ecl" => self.eye_color = value,
            "pid" => self.passport_id = value,
            "cid" => self.country_id = value,
            _ => (),
        }
    }

    fn is_valid_birth_year(&self) -> bool {
        let parsed = self.birth_year.parse::<usize>();
        if parsed.is_err() {
            return false;
        }
        match parsed.unwrap() {
            1920..=2002 => true,
            _ => false,
        }
    }

    fn is_valid_issue_year(&self) -> bool {
        let parsed = self.issue_year.parse::<usize>();
        if parsed.is_err() {
            return false;
        }
        match parsed.unwrap() {
            2010..=2020 => true,
            _ => false,
        }
    }

    fn is_valid_expiry_year(&self) -> bool {
        let parsed = self.expiration_year.parse::<usize>();
        if parsed.is_err() {
            return false;
        }
        match parsed.unwrap() {
            2020..=2030 => true,
            _ => false,
        }
    }

    fn is_valid_height(&self) -> bool {
        let unit;

        if !self.height.contains("in") && !self.height.contains("cm") {
            return false;
        }

        if self.height.contains("cm") {
            unit = "cm";
        } else {
            unit = "in";
        }

        let height = self
            .height
            .split(unit)
            .next()
            .unwrap()
            .parse::<usize>()
            .unwrap();

        match unit {
            "cm" => 193 >= height && height >= 150,
            "in" => 76 >= height && height >= 59,
            _ => false,
        }
    }

    fn is_valid_hair_color(&self) -> bool {
        if self.hair_color.len() != 7 {
            return false;
        }
        for (i, c) in self.hair_color.chars().enumerate() {
            if i == 0 {
                if c == '#' {
                    continue;
                } else {
                    return false;
                }
            }

            match c {
                '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '0' | 'a' | 'b' | 'c'
                | 'd' | 'e' | 'f' => (),
                _ => return false,
            }
        }

        return true;
    }

    fn is_valid_eye_color(&self) -> bool {
        match self.eye_color.as_str() {
            "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => true,
            _ => false,
        }
    }

    fn is_valid_pid(&self) -> bool {
        self.passport_id.len() == 9
    }
}

fn read_input(path: &str) -> String {
    fs::read_to_string(path).expect("Shit! Couldn't find file.")
}

fn parse_passport_from_lines(input: &str) -> Option<Passport> {
    let mut passport = Passport::empty();
    let mut data_values: Vec<&str> = Vec::new();

    // for each line, split on spaces to get a vector that looks like
    // ["byr:1990", "iyr:2020", ....]
    for line in input.lines() {
        let mut groups: Vec<&str> = line.split(' ').collect();
        data_values.append(&mut groups)
    }

    // TODO: Figure out the "rusty" way to do this
    for data in data_values.iter() {
        let d: Vec<&str> = data.split(':').collect();
        let record = d[0];
        let value = d[1];
        passport.add_record(record.to_string(), value.to_string());
    }

    if passport.is_filled_out() {
        Some(passport)
    } else {
        None
    }
}

fn part1() -> usize {
    let content = read_input("./input.txt");
    let mut valid_passports = 0;
    for record in content.split("\n\n") {
        let p = parse_passport_from_lines(record);
        match p {
            Some(passport) => {
                if passport.is_filled_out() {
                    valid_passports += 1;
                }
            }
            _ => (),
        }
    }

    valid_passports
}

fn part2() -> usize {
    let content = read_input("./input.txt");
    let mut valid_passports = 0;
    for record in content.split("\n\n") {
        let p = parse_passport_from_lines(record);
        match p {
            Some(passport) => {
                if passport.is_filled_out() && passport.is_correct() {
                    valid_passports += 1;
                }
            }
            _ => (),
        }
    }

    valid_passports
}

fn main() {
    let valid_passports = part1();
    println!("Valid Passports: {}", valid_passports);

    let correct_passports = part2();
    println!("Valid & Correct Passports: {}", correct_passports);
}

#[test]
fn test_parse_passport() {
    let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm";

    let p = parse_passport_from_lines(input);
    let is_passport = match p {
        Some(passport) => passport.is_filled_out(),
        _ => false,
    };

    assert!(is_passport);
}

#[test]
fn test_parse_invalid_passport() {
    let input = "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929";

    let p = parse_passport_from_lines(input);
    let is_passport = match p {
        Some(passport) => passport.is_filled_out(),
        _ => false,
    };

    assert_eq!(is_passport, false);
}

#[test]
fn test_missing_country_id() {
    let input = "hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm";

    let p = parse_passport_from_lines(input);
    let is_passport = match p {
        Some(passport) => passport.is_filled_out(),
        _ => false,
    };

    assert!(is_passport);
}

#[test]
fn test_missing_multiple_fields() {
    let input = "hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in";

    let p = parse_passport_from_lines(input);
    let is_passport = match p {
        Some(passport) => passport.is_filled_out(),
        _ => false,
    };

    assert_eq!(is_passport, false);
}

#[test]
fn test_valid_birth_year() {
    let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm";

    let p = parse_passport_from_lines(input);
    let valid_birth_year = match p {
        Some(passport) => passport.is_valid_birth_year(),
        _ => false,
    };

    assert!(valid_birth_year);

    let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:2020 iyr:2017 cid:147 hgt:183cm";

    let p = parse_passport_from_lines(input);
    let valid_birth_year = match p {
        Some(passport) => passport.is_valid_birth_year(),
        _ => false,
    };
    assert_eq!(valid_birth_year, false);
}

#[test]
fn test_is_valid_height() {
    let passport = Passport {
        birth_year: "".to_string(),
        issue_year: "".to_string(),
        expiration_year: "".to_string(),
        height: "59in".to_string(),
        hair_color: "".to_string(),
        eye_color: "".to_string(),
        passport_id: "".to_string(),
        country_id: "".to_string(),
    };

    let is_valid_height = passport.is_valid_height();
    assert!(is_valid_height);
}

#[test]
fn test_is_valid_hair() {
    let passport = Passport {
        birth_year: "".to_string(),
        issue_year: "".to_string(),
        expiration_year: "".to_string(),
        height: "".to_string(),
        hair_color: "#ffffff".to_string(),
        eye_color: "".to_string(),
        passport_id: "".to_string(),
        country_id: "".to_string(),
    };

    let is_valid_hair_color = passport.is_valid_hair_color();
    assert_eq!(is_valid_hair_color, true);
}
