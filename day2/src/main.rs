use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn input_to_lines(path: &str) -> Vec<String> {
    let contents = File::open(path).unwrap();
    let reader = BufReader::new(contents);

    let mut contents: Vec<String> = Vec::new();
    for line_ in reader.lines() {
        let line = line_.unwrap();
        contents.push(line);
    }
    contents
}

#[derive(Debug)]
struct Password {
    rule1: usize,
    rule2: usize,
    letter: char,
    password: String,
}

impl Password {
    fn new(rule1: usize, rule2: usize, letter: char, password: String) -> Self {
        Self {
            rule1,
            rule2,
            letter,
            password,
        }
    }

    fn is_valid_part1(&self) -> bool {
        let mut count = 0;
        for char_ in self.password.chars() {
            if char_ == self.letter {
                count += 1;
            }
        }

        // The first challenge works by rule1 being a "min" and rule2 being a "max"
        let min = self.rule1;
        let max = self.rule2;
        if count >= min && count <= max {
            true
        } else {
            false
        }
    }

    fn is_valid_part2(&self) -> bool {
        let first_char = self.password.chars().nth(self.rule1 - 1).unwrap();
        let second_char = self.password.chars().nth(self.rule2 - 1).unwrap();

        // The same letter cannot be in both positions
        if first_char == second_char {
            return false;
        }

        if first_char == self.letter || second_char == self.letter {
            true
        } else {
            false
        }
    }
}

fn parse_line(line: String) -> Password {
    let rules: Vec<&str> = line.split(|c| c == ' ' || c == '-' || c == ':').collect();
    let rule1 = rules[0].parse::<usize>().unwrap();
    let rule2 = rules[1].parse::<usize>().unwrap();
    let letter = rules[2].parse::<char>().unwrap();
    let password = rules[4].to_string(); // Weird that there's an empty string value in the third position

    Password::new(rule1, rule2, letter, password)
}

fn part1() -> usize {
    let mut valid_passwords = 0;
    let lines = input_to_lines("./input.txt");
    for l in lines {
        let password = parse_line(l);
        if password.is_valid_part1() {
            valid_passwords += 1;
        }
    }

    valid_passwords
}

fn part2() -> usize {
    let mut valid_passwords = 0;
    let lines = input_to_lines("./input.txt");
    for l in lines {
        let password = parse_line(l);
        if password.is_valid_part2() {
            valid_passwords += 1;
        }
    }

    valid_passwords
}

fn main() {
    let valid_passwords = part1();
    println!("Part 1 number of valid passwords: {}", valid_passwords);

    let valid_passwords = part2();
    println!("Part 2 number of valid passwords: {}", valid_passwords);
}

#[test]
fn test_validate_password_part1() {
    let password = Password::new(1, 3, 'a', "abcde".to_string());
    assert!(password.is_valid_part1());

    let password = Password::new(1, 3, 'b', "cdefg".to_string());
    assert!(password.is_valid_part1() == false);

    let password = Password::new(2, 9, 'c', "ccccccccc".to_string());
    assert!(password.is_valid_part1());
}

#[test]
fn test_validate_password_part2() {
    let password = Password::new(1, 3, 'a', "abcde".to_string());
    assert!(password.is_valid_part2(), "{:?}", password);

    let password = Password::new(1, 3, 'a', "cbade".to_string());
    assert!(password.is_valid_part2(), "{:?}", password);

    let password = Password::new(1, 3, 'b', "cdefg".to_string());
    assert!(password.is_valid_part2() == false);

    let password = Password::new(2, 9, 'c', "ccccccccc".to_string());
    assert!(password.is_valid_part2() == false);
}

#[test]
fn test_parse_line() {
    let line = "1-3 a: abcde";
    let password = parse_line(line.to_string());
    assert!(password.is_valid_part1(), "{:?}", password);
}
